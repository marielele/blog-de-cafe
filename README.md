# Blog del café
Diseño de páginas para un blog resonsive 
- langing page (index.html)
- Página acerca de la empresa/asociación (nosotros.html)
- Página de listado de cursos (cursos.html)
- Página de entrada del blog (entrada.html)
- Página de contacto con formulario (contacto.html)

## Creado con
- HTML5
- CSS3
#### y las librerías
- [NormalizeCSS](https://necolas.github.io/normalize.css/)
- [Modernizr](https://modernizr.com/)

URL: [Blog-del-café](https://cafe-coffee-blog.netlify.app/)